﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24MVC.Models;

namespace Task24MVC.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View("MyNewFirstPage");
        }
        public IActionResult CoachInfo()
        {
            Coach coach1 = new Coach { Name = "Marius", ID = 1, IsAvailable = true, Level = "Beginner" };
            Coach coach2 = new Coach { Name = "Henrik", ID = 2, IsAvailable = false, Level = "Pro" };
            Coach coach3 = new Coach { Name = "Pedro", ID = 3, IsAvailable = true, Level = "Pro" };

            List<Coach> coachlist = new List<Coach>();
            coachlist.Add(coach1);
            coachlist.Add(coach2);
            coachlist.Add(coach3);


            return View(coachlist);
        }

    }
}
